<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'trivium' );

/** Database username */
define( 'DB_USER', 'user' );

/** Database password */
define( 'DB_PASSWORD', 'mdp' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@IG]*zp):2BM@1S};0CyC2?j~#p``_LiwB|c+i)T#=rB:K8!iDAAgFUg|nh:nb@)' );
define( 'SECURE_AUTH_KEY',  '}n2uw!IFhTIS8tK0~Z$[,$#py.iVSO{N<wiGH;{+h6v93O2j0SRnL94j#^7asXmU' );
define( 'LOGGED_IN_KEY',    's)N>`E[VpyhRlsYl:82i/oH|1@xk>Q,e7mPCv[+mVEu$Q%gAJQ+k`Y86-E(643K$' );
define( 'NONCE_KEY',        'D)+CR^Mc([V%0y#)dz`Kz;R|0r<8 IE+5KwN^K55$RST 8QGI%,62^IDek1Ig!4v' );
define( 'AUTH_SALT',        'v#5-#`!nX$+RiAM-ZN6[0]XSI}[? CM]4SY O]>X>1S[DVU?CcPxf-t%|,@{,khb' );
define( 'SECURE_AUTH_SALT', 'uN)VHTQtW}d/Q>+Z+OQjLh7f7s9z~.sy|?)|Xt2imUQ]Nz1lo`!b86LnPq0y`HE)' );
define( 'LOGGED_IN_SALT',   '7@tfF^|o80^g[#I 6Pg2yQ^k8FK-{C[#(y%M-bHT^|1,_J5I1U|4.vYS[0XP%Vb,' );
define( 'NONCE_SALT',       'hWb`8_WK[Go7%=e2z0+N>l&qTIm~Xg0d{D(}:u4TYs3}hj=sLU@@9lxC50nlPR@K' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */

define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
