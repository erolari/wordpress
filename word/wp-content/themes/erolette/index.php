<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="wp-content/themes/erolette/style.css">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />
<?php wp_head(); ?>
</head>
<body>
    
<h1><?php bloginfo( 'name' ); ?></h1>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<h3><?php the_title(); ?></h3>
<img src="<?php echo get_field('image') ?>" alt="">
<a href="<?php echo get_permalink() ?>"> <?php echo the_title() ?></a>



<?php endwhile; ?>

<?php
if ( get_next_posts_link() ) {
next_posts_link();
}
?>
<?php
if ( get_previous_posts_link() ) {
previous_posts_link();
}
?>

<?php else: ?>

<p>No posts found. :(</p>

<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>